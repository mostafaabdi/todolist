from django.urls import path
from .views import TodoListView, TodoDetailView, TodoCreateView, TodoUpdateView, TodoDeleteView, TodayListView, \
    TodoSearchView, TagFilterView, TagCreateView, TagListView, TagUpdateView, TagDeleteView

app_name = 'todoapp'
urlpatterns = [
    path('', TodoListView.as_view(), name='ListView'),
    path('today', TodayListView.as_view(), name='TodayView'),
    path('<int:pk>', TodoDetailView.as_view(), name='DetailView'),
    path('create/', TodoCreateView.as_view(), name='CreateView'),
    path('update/<int:pk>', TodoUpdateView.as_view(), name='UpdateView'),
    path('delete/<int:pk>', TodoDeleteView.as_view(), name='DeleteView'),
    path('search/', TodoSearchView.as_view(), name='SearchView'),
    path('search/tag=<str:tag>/', TagFilterView.as_view(), name='TagsFilterView'),
    path('tags/create', TagCreateView.as_view(), name='TagCreateView'),
    path('tags/', TagListView.as_view(), name='TagListView'),
    path('tags/update/<int:pk>', TagUpdateView.as_view(), name='TagUpdateView'),
    path('tags/delete/<int:pk>', TagDeleteView.as_view(), name='TagDeleteView'),
]
