from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, TodayArchiveView
from .models import Todolist, Tag
from django.urls import reverse_lazy
from .forms import TodoCreateFrom, TagUpdateForm, TagCreateFrom


class TodoListView(ListView):
    model = Todolist
    paginate_by = 8
    template_name = 'todoapp/todo_list_view.html'


class TagFilterView(ListView):
    def get_context_data(self, **kwargs):
        context = super(TagFilterView, self).get_context_data(**kwargs)
        context.update({
            'tags': Tag.objects.all(),
        })
        return context

    def get_queryset(self):
        query_list = self.model.objects.filter(tags__title__icontains=self.kwargs['tag']).order_by('-id')
        return query_list

    paginate_by = 2
    model = Todolist
    template_name = 'todoapp/todo_search_view.html'


class TagCreateView(CreateView):
    model = Tag
    form_class = TagCreateFrom
    template_name = 'todoapp/tag_create_view.html'
    success_url = reverse_lazy('todoapp:TagListView')


class TagListView(ListView):
    model = Tag
    paginate_by = 50
    template_name = 'todoapp/tag_list_view.html'
    context_object_name = 'tags'

    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)
        related_tasks = {}
        tasks = []
        """Create a dictionary named related_tasks which it's keys are the tag ids and the values are 
        the title of tasks of the related tag.
        example:  related_tasks = {'tag1':['task1', 'task2'], 'tag2:['task1', 'task3', 'task5']}"""
        for tag in Tag.objects.all():
            for task in tag.todolist_set.all():
                tasks.append(task.title)
            related_tasks[tag.id] = tasks
            tasks = []
        context.update({
            'related_tasks': related_tasks,
        })
        return context


class TagUpdateView(UpdateView):
    model = Tag
    form_class = TagUpdateForm
    template_name = 'todoapp/tag_update_view.html'
    success_url = reverse_lazy('todoapp:TagListView')


class TagDeleteView(DeleteView):
    model = Tag
    template_name = 'todoapp/tag_delete_view.html'
    success_url = reverse_lazy('todoapp:TagListView')


class TodoSearchView(ListView):

    def get_context_data(self, **kwargs):
        context = super(TodoSearchView, self).get_context_data(**kwargs)
        context.update({
            'tags': Tag.objects.all(),
        })
        return context

    def get_queryset(self):
        query_list = self.model.objects.all()
        query = self.request.GET.get('search')
        if query:
            query_list = self.model.objects.filter(title__icontains=query).order_by('-id')
        return query_list

    paginate_by = 2
    model = Todolist
    template_name = 'todoapp/todo_search_view.html'


class TodayListView(TodayArchiveView):
    model = Todolist
    paginate_by = 2
    date_field = 'created_at'
    template_name = 'todoapp/today_list_view.html'


class TodoDetailView(DetailView):
    model = Todolist
    template_name = 'todoapp/todo_detail_view.html'


class TodoCreateView(CreateView):
    model = Todolist
    form_class = TodoCreateFrom
    template_name = 'todoapp/todo_create_view.html'


class TodoUpdateView(UpdateView):
    model = Todolist
    form_class = TodoCreateFrom
    template_name = 'todoapp/todo_update_view.html'


class TodoDeleteView(DeleteView):
    model = Todolist
    success_url = reverse_lazy('todoapp:ListView')
    template_name = 'todoapp/todo_delete_view.html'
