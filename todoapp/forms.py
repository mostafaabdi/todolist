from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import InlineCheckboxes
from crispy_forms.layout import Submit, Layout, Row, Column, HTML
from .models import Todolist, Tag


class TagCreateFrom(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['title']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save'))
        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group  col-md-4 mb-0'),
            ),
        )


class TagUpdateForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['title']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save'))
        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group  col-md-4 mb-0'),
            ),
        )


class TodoCreateFrom(forms.ModelForm):
    due_date = forms.DateField(
        widget=forms.TextInput(
            attrs={'type': 'date'}
        )
    )

    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(), widget=forms.CheckboxSelectMultiple)

    class Meta:
        model = Todolist
        fields = ['title', 'description', 'priority', 'due_date', 'tags', 'is_done', 'color']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save'))
        self.helper.layout = Layout(

            Row(
                Column('title', css_class='form-group  col-md-4 mb-0'),
                Column('priority', css_class='form-group col-md-4 mb-0'),
                Column('due_date', css_class='form-group col-md-4 mb-0'),

            ),
            Row(
                Column('description', css_class='form-group  col-md-8 mb-0'),
                Column('color', css_class='form-group col-md-4 mb-0 '),
            ),
            'is_done',
            InlineCheckboxes(
                'tags', ''
            ),
        )
