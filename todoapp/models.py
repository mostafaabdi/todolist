from colorfield.fields import ColorField
from django.db import models
from django.db.models import Deferrable, UniqueConstraint
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from .choices import TodoPriority, TODO_COLOR_CHOICES


class Tag(models.Model):
    title = models.CharField(_('title'), max_length=40)

    def __str__(self):
        return self.title

    class Meta:
        constraints = [
            UniqueConstraint(name='unique_title', fields=['title'], deferrable=Deferrable.DEFERRED, )
        ]

    def save(self, *args, **kwargs):
        self.title = self.title.replace(' ', '_')
        super(Tag, self).save(*args, **kwargs)


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Todolist(TimeStampMixin):
    title = models.CharField(_('title'), max_length=250, null=False, blank=False)
    description = models.TextField(_('description'), null=False, blank=False)
    priority = models.CharField(_('priority'), max_length=50, choices=TodoPriority.choices, default=TodoPriority.MEDIUM)
    is_done = models.BooleanField(_('is done?'), default=False)
    due_date = models.DateField(_('due date'), default=timezone.now().date())
    color = ColorField(choices=TODO_COLOR_CHOICES, blank=True)
    tags = models.ManyToManyField('Tag')

    @property
    def is_expired(self):
        return self.due_date < timezone.now().date()

    def __str__(self):
        days = (self.due_date - timezone.now().date()).days
        if days == 0:
            return self.title + ' | ' + self.priority.upper() + " | Today"
        if days == -1:
            return self.title + ' | ' + self.priority.upper() + " | Yesterday"
        if days == 1:
            return self.title + ' | ' + self.priority.upper() + " | Tomorrow"
        if days < 0:
            return self.title + ' | ' + self.priority.upper() + " | {0} days ago".format(str(abs(days)))
        else:
            return self.title + ' | ' + self.priority.upper() + " | next {0} days".format(str(days))

    def get_absolute_url(self):
        return reverse('todoapp:ListView')
