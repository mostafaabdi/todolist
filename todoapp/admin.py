from django.contrib import admin
from .models import Todolist, Tag

admin.site.register(Todolist)
admin.site.register(Tag)
