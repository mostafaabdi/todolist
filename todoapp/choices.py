from django.db import models
from django.utils.translation import gettext_lazy as _


class TodoPriority(models.TextChoices):
    HIGH = _('high')
    MEDIUM = _('medium')
    LOW = _('low')


TODO_COLOR_CHOICES = {
    ("#FFFFFF", _("white")),
    ("#FFFF00", _("yellow")),
    ("#ff5733", _("orange")),
    ("#FF0000", _("red")),
    ("#FF00FF", _("pink")),
    ("#800080", _("purple")),
    ("#008000", _("green")),
    ("#0000FF", _("blue")),
}
